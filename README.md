# Angular NGXS example

During a course on NGXS, I worked on an Angular project that showcase of how NGXS can enhance the state management of an application. (Same base code than ng-ngrx-ex repository)

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.
